import styles from "../styles/Home.module.css";
import { Table, Thead, Tbody, Tr, Th, Td } from "@chakra-ui/react";
import {
  gql,
  useQuery,
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
} from "@apollo/client";
import React, { useState } from "react";

export default function Home() {
  const [ischecked, setischecked] = useState(false);
  const client = new ApolloClient({
    uri: "https://countries.trevorblades.com/",
    cache: new InMemoryCache(),
  });

  const COUNTRYLIST = gql`
    query countries {
      countries {
        code
        name
        emoji
        capital
      }
    }
  `;

  function CountryListing() {
    const { loading, error, data } = useQuery(COUNTRYLIST);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :()</p>;

    return data.countries.map(({ code, name, emoji, capital }) => (
      <Tr key={code}>
        <Td w={"5%"} minW={40}>
          <input checked={ischecked} type="checkbox" />
        </Td>
        <Td w={"40%"} minW={180}>
          {name}
        </Td>
        <Td w={"25%"} minW={180}>
          {capital}
        </Td>
        <Td w={"25%"} minW={180}>
          {code}
        </Td>
        <Td w={"5%"} minW={40}>
          <p role="img" aria-label="hamburger">
            {emoji}
          </p>
        </Td>
      </Tr>
    ));
  }
  return (
    <ApolloProvider client={client}>
      <div className={styles.container}>
        <div className={styles.tableWrapper}>
          <div style={{ overflow: "auto" }}>
            <div style={{ padding: 10 }}>
              <Table
                variant="simple"
                size="lg"
                w="100%"
                className="responsiveTable"
              >
                <Thead>
                  <Tr textAlign="left">
                    <Th w={"5%"} minW={40}>
                      <input
                        type="checkbox"
                        checked={ischecked}
                        onChange={() => setischecked(!ischecked)}
                      />
                    </Th>
                    <Th w={"40%"} minW={180}>
                      Country Name
                    </Th>
                    <Th w={"25%"} minW={180}>
                      Country capital
                    </Th>
                    <Th w={"25%"} minW={180}>
                      Country Code
                    </Th>
                    <Th w={"5%"} minW={40}>
                      {" "}
                      emoji
                    </Th>
                  </Tr>
                </Thead>
              </Table>
            </div>
            <div
              className="tableBodySTy"
              style={{
                background: "#F8F6F8",
                padding: 10,
                borderRadius: 10,
                marginTop: 20,
                width: "fit-content",
                width: "fit-content",
                minWidth: "100%",
              }}
            >
              <Table
                variant="simple"
                size="lg"
                w="100%"
                className="responsiveTable"
              >
                <Tbody>
                  <CountryListing />
                </Tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    </ApolloProvider>
  );
}
